# HOW TO UNINSTALL

You know how to...

```bash
rm -rf ~/path-to-repo
```

See you in a next time ;)


---

[<< previous](create_a_merge_request.md) | [next >>](./../../AUTHOR.md)

[HOME](./../../README.md)
