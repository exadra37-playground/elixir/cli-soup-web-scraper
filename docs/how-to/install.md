# HOW TO INSTALL

Just clone it into a location in your computer...

```bash
git clone git@gitlab.com:exadra37-playground/elixir/cli-soup-web-scraper.git
```


---

[<< previous](./../../README.md) | [next >>](use.md)

[HOME](./../../README.md)
