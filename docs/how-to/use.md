# HOW TO USE


From the inside the repo you have cloned in the install instructions:

```bash
cd ~/path-to-repo/src/soup
```

Now just check the built in help...

```bash
./soup --help
```

Have fun ;)


---

[<< previous](install.md) | [next >>](./../../CONTRIBUTING.md)

[HOME](./../../README.md)
