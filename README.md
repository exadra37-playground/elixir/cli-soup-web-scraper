# CLI Soup Web Scraper

My first application in Elixir... hooray :)

Achieved thanks to [this nice tutorial](http://softwarebysteve.com/building-a-command-line-web-scraper-in-elixir.html).

This CLI command makes use of following Elixir packages:

* [Floki](https://hex.pm/packages/floki) for web scrape.
* [HttpPoison](https://hex.pm/packages/httpoison) for http requests.


## MENU

* **How To**
    + [Install](./docs/how-to/install.md)
    + [Use](./docs/how-to/use.md)
    + [Report an Issue](./docs/how-to/create_an_issue.md)
    + [Create a Branch](./docs/how-to/create_branches.md)
    + [Open a Merge Request](./docs/how-to/create_a_merge_request.md)
    + [Uninstall](./docs/how-to/uninstall.md)
* **Road Map**
    + [Milestones](https://gitlab.com/exadra37-playgroung/elixir/cli-web-scraper/milestones)
    + [Overview](https://gitlab.com/exadra37-playgroung/elixir/cli-web-scraper/boards)
* **About**
    + [Author](AUTHOR.md)
    + [Contributors](CONTRIBUTORS.md)
    + [Contributing](CONTRIBUTING.md)
    + [Project](DESCRIPTION.md)
    + [License](LICENSE)


## SUPPORT DEVELOPMENT

If this is useful for you, please:

* Share it on [Twitter](https://twitter.com/home?status=https%3A//gitlab.com/exadra37-playground/elixir/cli-web-scraper%20by%20%40Exadra37.%20Building%20CLI%20web%20scraper%20while%20playing%20with%20%23ElixirLang%20for%20learning%20purposes.%20%23myelixirstatus%20%23elixir) or in any other channel of your preference.
* Consider to [offer me](https://www.paypal.me/exadra37) a coffee, a beer, a dinner or any other treat 😎.


## EXPLICIT VERSIONING

This repository uses [Explicit Versioning](https://gitlab.com/exadra37-versioning/explicit-versioning) schema.


## BRANCHES

Branches are created as demonstrated [here](docs/how-to/create_branches.md).

This are the type of branches we can see at any moment in the repository:

* `master` - issues and milestones branches will be merge here.
* `last-stable-release` - matches the last stable tag created. Useful for automation tools.
* `issue-4_fix-email-validation` (issue-number_title) - each issue will have is own branch for development.
* `milestone-12_add-cache` (milestone-number_title) - all Milestone issues will start, track and merged here.

Only `master` and `last-stable-release` branches will be permanent ones in the repository and all other ones will be
removed once they are merged.
